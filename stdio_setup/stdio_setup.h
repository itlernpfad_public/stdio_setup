/* SPDX-License-Identifier: Apache-2.0
 * 
 * stdio_setup
 * Copyright 2005 - 2021 The stdio_setup Authors
 * Copyright 2021 ITlernpfad
 * 
 * ============================================================================
 * ============================================================================
 * This software contains software derived from portions of Stdio demo
 * (uart.h, defines.h) by Joerg Wunsch, with various modifications by ITlernpfad and 
 * other stdio_setup authors, see stdio_setup AUTHORS file.
 * 
 * Stdio demo (https://svn.savannah.gnu.org/viewvc/avr-libc/trunk/avr-libc/doc/examples/stdiodemo/)
 * Copyright 2005 - 2010 Joerg Wunsch <joerg@FreeBSD.ORG>
 * Licensed under THE BEER-WARE LICENSE (Revision 42)
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <joerg@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.        Joerg Wunsch
 * ----------------------------------------------------------------------------
 *
 * uart
 *
 * $Id: uart.h 1008 2005-12-28 21:38:59Z joerg_wunsch $
 * 
 */
 
/*
 * @package stdio_setup
 * @file src/stdio_setup.h 
 * @authors 2021 ITlernpfad and otherstdio_setup authors
 * (https://gitlab.com/ITlernpfad_Public/stdio_setup/-/blob/master/AUTHORS)
 * @copyright 2005 - 2021 The stdio_setup Authors
 * Copyright 2021 ITlernpfad
 * Copyright 2005 Joerg Wunsch
 * @brief Part of uart
 * @details stdio_setup is a USB serial communication library written in C.  
 * language: C99
 * status: Beta
 * @version 1.0.2
 */

#ifndef UART_H
#define UART_H

#include <stdio.h>

/* CPU frequency */
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

/* UART baud rate */
#define UART_BAUD  9600

/* Whether to read the busy flag, or fall back to
   worst-time delays. */
#define USE_BUSY_BIT 1

/*
 * Perform UART startup initialization.
 */
void	UartInit(void);

/*
 * Send one character to the UART.
 */
int	uart_putchar(char c, FILE *stream);

/*
 * Size of internal line buffer used by uart_getchar().
 */
#define RX_BUFSIZE 80

/*
 * Receive one character from the UART.  The actual reception is
 * line-buffered, and one character is returned from the buffer at
 * each invokation.
 */
int	uart_getchar(FILE *stream);

#endif /* UART_H */
