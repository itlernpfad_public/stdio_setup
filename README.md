# stdio_setup

stdio_setup.h is a header file providing fuctions of the stdio_setup module of the stdio_setup library for serial communication between Microchip AVR microcontroller and personal computer via USB Serial using UART

## Using stdio_setup
To use the functions provided by the library stdio_setup, download stdio_setup and unzip it in a directory that is scanned for libraries by the linker of the compiler (or point the linker to the directory of the stdio_setup directory).
If you use PlatformIO, this is done by creating a C project and making sure that the following lines are present in the platformio.ini file of the project:
```
lib_deps =
  https://gitlab.com/itlernpfad_public/stdio_setup.git
```  

The following example that is written in the programming language C illustrates how to use the stdio_setup library: 
```
#include <stdio.h> 

/* 
 * Provide function InitUart to initialize UART for transmission of stdio and stderr streams. 
 */
#include "stdio_setup.h" 

int main(void) {

  /* 
   * Initialize UART for transmission of stdio and stderr input/output streams using polling.
   * On Arduino Uno-compatible development boards, this directs stdio and stderr data via UART to USB.
   */
  UartInit();

  /*
   * Send data to the stdio input/output stream
   */
  printf("hello world!");

  return 0;
}
```

## License information

```
Copyright 2005 - 2021 The stdio_setup authors
Copyright 2021 ITlernpfad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



This software contains software derived from Stdio demo
(https://svn.savannah.gnu.org/viewvc/avr-libc/trunk/avr-libc/doc/examples/stdiodemo/), 
with various modifications by ITlernpfad and other stdio_setup authors, 
see stdio_setup AUTHORS file.

Stdio demo (https://svn.savannah.gnu.org/viewvc/avr-libc/trunk/avr-libc/doc/examples/stdiodemo/)
Copyright 2010 aboyapati
Copyright 2005 - 2010 Joerg Wunsch <joerg@FreeBSD.ORG>
License: THE BEER-WARE LICENSE (Revision 42) or Modified BSD License

"THE BEER-WARE LICENSE" (Revision 42):
<joerg@FreeBSD.ORG> wrote this file.  As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.        Joerg Wunsch

```
