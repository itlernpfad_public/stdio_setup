/* SPDX-License-Identifier: Apache-2.0
 * 
 * stdio_setup
 * Copyright 2005 - 2020 The stdio_setup authors
 * Copyright 2020 ITlernpfad
 * 
 * ============================================================================
 * ============================================================================
 * This software contains software derived from portions of Stdio demo
 * (uart.h, defines.h) by Joerg Wunsch, with various modifications by ITlernpfad and 
 * other stdio_setup authors, see stdio_setup AUTHORS file.
 * 
 * Stdio demo (https://www.nongnu.org/avr-libc//user-manual/group__stdiodemo.html)
 * Copyright 2005 - 2010 Joerg Wunsch <joerg@FreeBSD.ORG>
 * Licensed under THE BEER-WARE LICENSE (Revision 42)
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <joerg@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.        Joerg Wunsch
 * ----------------------------------------------------------------------------
 *
 * uart
 *
 * $Id: uart.h 1008 2005-12-28 21:38:59Z joerg_wunsch $
 * 
 */

/*
 * @package stdio_setup
 * @file examples/hello/hello.c 
 * @package hello
 * @authors 2020 ITlernpfad and other stdio_setup authors
 * (https://gitlab.com/ITlernpfad_Public/uart/-/blob/master/AUTHORS)
 * @copyright 2005 - 2020 The stdio_setup authors
 * Copyright 2020 ITlernpfad
 * Copyright 2005 Joerg Wunsch
 * @brief Part of stdio_setup
 * @details stdio_setup is a USB serial communication library written in C.  
 * language: C99
 * status: Beta
 * @version 1.0.2
 */

#include <stdio.h>
#include "stdio_setup.h"

int main(void)
{
	UartInit();

	printf("Hello, world!\r\n");

	for(;;) {
	}
    return 0;
}
